import sys
import zipfile
from StringIO import StringIO
import os

def zippegize(imageFilename, filenames):
    zipScratch = StringIO()
    zip = zipfile.ZipFile(zipScratch, 'w')
    for fileToZip in filenames:
        zip.write(fileToZip)
    zip.close()

    zipScratch.seek(0)

    imageFilenameOnly, imageExtension = os.path.splitext(imageFilename)
    imageScratch = open(imageFilename, "rb")
    
    resultFile = open(imageFilenameOnly + ".zip" + imageExtension, "wb")
    
    resultFile.write(imageScratch.read())
    resultFile.write(zipScratch.getvalue())

    zipScratch.close()
    imageScratch.close()
    resultFile.close()


if __name__ == "__main__":
    if (len(sys.argv) > 2):
        imageFilename = sys.argv[1]
        toZipFilenames = sys.argv[2:]

        zippegize(imageFilename, toZipFilenames)
