import unittest
import zipfile
from zippeg import zippegize

# note to self: run with:
# python -m unittest unittestZippeg

class TestZippegize(unittest.TestCase):
    
    #def setUp(self):
    #    self.a = -1

#
# test:
#
# create a zipeg and extract contents
# ensure the contents are the same as original files
#
# TODO: ensure the first n bytes of the zipeg is the same
#  as the original image, where n is the number of bytes
#  the original image has
#

    def testZippegize(self):
        zippegize("./cateyes.test.png", ["./zippeg.py", "unittestZippeg.py"]);

        zip = zipfile.ZipFile("./cateyes.test.zip.png");

        zip.testzip()

        zippegArchived = zip.read("zippeg.py")
        zippegFile = open("./zippeg.py", "r").read()

        self.assertEqual(zippegArchived, zippegFile)


        unittestZippegArchived = zip.read("unittestZippeg.py")
        unittestZippegFile = open("./unittestZippeg.py", "r").read()

        self.assertEqual(unittestZippegArchived, unittestZippegFile)
        
        zip.close()
        

#if __name__ == "__main__":
#    unittest.main()

suite = unittest.TestLoader().loadTestsFromTestCase(TestZippegize)
unittest.TextTestRunner(verbosity=2).run(suite)
