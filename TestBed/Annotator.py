import wx


class AnnotatorApp(wx.App):
	def __init__(self, redirect=False, filename=None):
		wx.App.__init__(self, redirect, filename)
		self.frame = wx.Frame(None, title="Open Image Annotation Initiative: Annotator")

		self.CreateWidgets()
		self.frame.Show()

	def CreateWidgets(self):
		self.frame.CreateStatusBar()
		self.CreateMenu()

		self.panel = wx.Panel(self.frame)

		self.originalImage = wx.EmptyImage(320, 240)
		self.image = self.originalImage
		self.imageDisplay = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(self.image))
		#self.imageDisplay.Centre()

		self.Bind(wx.EVT_SIZE, self.OnResize)

	def GetWindowSize(self):
		size = self.frame.GetClientSizeTuple()
		#print size
		if (size[0] < size[1]):
			return size[0]
		else:
			return size[1]

	def CreateMenu(self):
		fileMenu = wx.Menu()
		#fileMenu.Append(wx.ID_ABOUT, "&About", "Information about this program")
		openItem = fileMenu.Append(wx.ID_OPEN, "&Open...", "Open an image")
		fileMenu.Append(wx.ID_SAVE, "&Save", "Save annotations to the image")
		fileMenu.Append(wx.ID_SAVEAS, "S&ave as...", "Save image under a different filename")
		fileMenu.Append(wx.ID_EXIT, "E&xit", "Terminate this program")

		self.frame.Bind(wx.EVT_MENU, self.OnOpen, openItem)

		editMenu = wx.Menu()
		editMenu.Append(wx.ID_UNDO, "&Undo", "Undo last change")
		editMenu.Append(wx.ID_REDO, "&Redo", "Redo undone change")

		viewMenu = wx.Menu()
		viewMenu.Append(wx.ID_ZOOM_IN, "Zoom &in", "")
		viewMenu.Append(wx.ID_ZOOM_OUT, "Zoom &out", "")
		viewMenu.Append(wx.ID_ZOOM_100, "&Actual Size", "")
		viewMenu.Append(wx.ID_ZOOM_FIT, "&Fit to window", "")

		windowMenu = wx.Menu()

		menuBar = wx.MenuBar()
		menuBar.Append(fileMenu, "&Image")
		#menuBar.Append(editMenu, "&Edit")

		self.frame.SetMenuBar(menuBar)

	def OnOpen(self, event):
		wildcard = "Images|*.jpg;*.jpeg;*.png;*.gif"
		openDialog = wx.FileDialog(None, "Open Image", wildcard=wildcard, style=wx.OPEN)
		if openDialog.ShowModal() == wx.ID_OK:
			self.OpenImage(openDialog.GetPath())
		openDialog.Destroy()

	def OpenImage(self, filepath):
		self.frame.SetTitle(filepath)
		self.originalImage = wx.Image(filepath, wx.BITMAP_TYPE_ANY)
		self.image = self.originalImage
		self.ResizeImage()

	def OnResize(self, event):
		#print "resize"
		self.ResizeImage()

	def ResizeImage(self):
		w = self.image.GetWidth()
		h = self.image.GetHeight()

		if w > h:
			newW = self.GetWindowSize()
			newH = self.GetWindowSize() * h/w
		else:
			newW = self.GetWindowSize() * w/h
			newH = self.GetWindowSize()

		#print "%d %d" % (newW, newH)

		self.image = self.originalImage.Scale(newW, newH)

		#print self.image

		self.imageDisplay.SetBitmap(wx.BitmapFromImage(self.image))
		#self.imageDisplay.Centre()
		self.panel.Refresh()

if __name__ == "__main__":
	app = AnnotatorApp()
	app.MainLoop()

